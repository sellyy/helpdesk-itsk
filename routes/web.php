<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\TiketController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ExportController;
use App\Http\Controllers\UmpanBalikController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/*
|--------------------------------------------------------------------------
| Halaman Landing Page
|--------------------------------------------------------------------------
*/
// Login
Route::get('/', [LoginController::class, 'index']);
Route::get('/login', [LoginController::class, 'login']);
Route::post('/autentikasi', [LoginController::class, 'autentikasi']);
Route::get('/status', function () {
    return view('status');
});

// tampilan dashboard tiap role
Route::get('/dashboard_admin', [LoginController::class, 'admin'])->middleware('Admin');
Route::get('/dashboard_superadmin', [LoginController::class, 'superadmin'])->middleware('SuperAdmin');
Route::get('/dashboard_teknisi', [LoginController::class, 'teknisi'])->middleware('Teknisi');

// logout
Route::post('/logout', [LogoutController::class, 'logout']);

// lupa sandi
Route::get('/lupa_sandi', [LoginController::class, 'lupasandi']);

Route::post('/password_email', [LoginController::class, 'ubahpassword']);

Route::get('/reset-password/{token}', function (string $token) {
    return view('auth.reset-password', ['token' => $token]);
})->name('password.reset');

Route::post('/reset-password', [LoginController::class, 'resetpassword'])->name('password.update');
// Route::post('logout',LogoutController::class)->name('logout');
// Route::get('/', function () {
//     return view('home');
// });
Route::get('/umpan-balik', function () {
    return view('umpan-balik');
});
Route::post('/tambah-umpan-balik', [UmpanBalikController::class, 'tambahUmpanBalik'])
    ->name('tambah.umpan_balik');

/*
|--------------------------------------------------------------------------
| Halaman Form Lapor
|--------------------------------------------------------------------------
*/
Route::get('/lapor-{role}', [TiketController::class, 'index'])->name('lapor.index');
Route::post('/lapor-{role}', [TiketController::class, 'store'])->name('lapor.store');
/*
|--------------------------------------------------------------------------
| Halaman Super Admin
|--------------------------------------------------------------------------
*/
Route::get('/superadmin/dashboard', function () {
    return view('superadmin.dashboard');
})->name('sprdashboard');

Route::get('/superadmin/tiket', function () {
    return view('superadmin.tiket');
})->name('sprtiket');

Route::get('/superadmin/status', function () {
    return view('superadmin.status_tiket');
})->name('sprstatustiket');

Route::get('/superadmin/kategori-masalah', function () {
    return view('superadmin.kategori_masalah');
})->name('sprkategorimasalah');

Route::get('/superadmin/unit', function () {
    return view('superadmin.unit');
})->name('sprunit');

Route::get('/superadmin/pengguna', function () {
    return view('superadmin.pengguna');
})->name('sprpengguna');

Route::get('/superadmin/profil', function () {
    return view('superadmin.profil');
})->name('sprprofil');


/*
|--------------------------------------------------------------------------
| Halaman Admin
|--------------------------------------------------------------------------
*/
Route::get('/admin/dashboard', [DashboardController::class, 'lihatTiket'])->name('admdashboard');
Route::get('/generate-excel', [ExportController::class, 'generateExcel']);
Route::get('/admin/tiket-mahasiswa', function () {
    return view('admin.tiket_mahasiswa');
})->name('admtiketmahasiswa');

Route::get('/admin/tiket-dosen', function () {
    return view('admin.tiket_dosen');
})->name('admtiketdosen');

Route::get('/admin/tiket-karyawan', function () {
    return view('admin.tiket_karyawan');
})->name('admtiketkaryawan');

Route::get('/admin/profil', function () {
    return view('admin.profil');
})->name('admprofil');


/*
|--------------------------------------------------------------------------
| Halaman Teknisi
|--------------------------------------------------------------------------
*/

Route::get('/teknisi', function () {
    return view('teknisi.dashboard');
})->name('tksdashboard');

Route::get('/teknisi/dashboard_teknisi', function () {
    return view('teknisi.dashboard');
})->name('tksdashboard');

Route::get('/teknisi/dashboard', function () {
    return view('teknisi.dashboard');
})->name('tksdashboard');

Route::get('/teknisi/aduan-belum-proses', function () {
    return view('teknisi.aduan_belumproses');
})->name('tksaduanbelumproses');

Route::get('/teknisi/aduan-proses', function () {
    return view('teknisi.aduan_proses');
})->name('tksaduanproses');

Route::get('/teknisi/aduan-selesai', function () {
    return view('teknisi.aduan_selesai');
})->name('tksaduanselesai');

Route::get('/teknisi/profil', function () {
    return view('teknisi.profil');
})->name('tksprofil');

/*
|--------------------------------------------------------------------------
| Halaman Authentikasi
|--------------------------------------------------------------------------
*/
// Route::get('/login', function () {
//     return view('auth.login');
// })->name('login');

// Route::get('/lupa-sandi', function () {
//     return view('auth.forgot-password');
// })->name('forgot-password');

Route::get('/reset-sandi', function () {
    return view('auth.reset-password');
})->name('reset-password');

Route::get('/reset-sukses', function () {
    return view('auth.reset-sukses');
})->name('reset-sukses');
