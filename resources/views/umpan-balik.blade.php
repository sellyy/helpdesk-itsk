<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
    @vite(['resources/css/app.scss', 'resources/js/app.js'])
    <title>Umpan Balik</title>
</head>

<body>
    @include('partial.navbar')
    <section id="umpanBalik" class="container-fluid mt-2 pt-5">
        <div class="w-full h-full">
            <img src="assets/images/bg-section-feedback.png" id="image-status" class="img-fluid" alt="">
        </div>
    </section>
    <section id="form-umpan-balik" class="container-fluid pt-5 mt-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="div-w-layout-hflex"><br>
                        <img src="assets/images/hubungi.png" alt=""><br><br>
                        <img src="assets/images/sampaikan-umpan-balik.png" alt="">
                    </div>
                    <p> <br>Terimakasih telah berkenan membuat umpan balik untuk kami, <br>tindakan anda sangat membantu kami.</p>
                    <div class="text-wrapper-2"><img src="assets/images/maps-icon.png" alt="Maps Icon"> Jl. S. Supriadi No.22, Sukun, Kec. Sukun, Kota Malang, Jawa Timur 65147</div>
                    <div class="link-info"><img src="assets/images/email-icon.png" alt="Email Icon"> itskhelp@itskdrsprn.ac.id</div><br><br>
                </div>
                <div class="col">
                    <form action="{{ route('tambah.umpan_balik') }}" method="POST">
                        @csrf
                        <div class="row mt-3">
                            <div class="col md-4">
                                <label for="nama">Nama</label><br>
                                <input type="text" id="nama" name="nama" style="width: 300px; height: 50px" placeholder="Nama Kamu">
                            </div>
                            <div class="col md-4">
                                <label for="status">Status</label><br>
                                <input type="text" id="status" name="status" style="width: 300px; height: 50px" placeholder="Mahasiswa/Dosen/Karyawan">
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col md-4">
                                <label for="email">Email</label><br>
                                <input type="email" id="email" name="email" style="width: 300px; height: 50px" placeholder="Alamat Email">
                            </div>
                            <div class="col md-4">
                                <label for="telepon">Nomor Telepon</label><br>
                                <input type="text" id="telepon" name="no_telepon" style="width: 300px; height: 50px" placeholder="No Telp">
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col md-4">
                                <label for="pesan">Pesan</label><br>
                                <input type="text" id="pesan" name="pesan" style="width: 630px; height: 100px" placeholder="Kritik & Saran">
                            </div>
                        </div>
                        <button type="submit" class="button-feedback">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </section>

    @include('partial.footer')
    <!-- 
    <script src="https://kit.fontawesome.com/c3621d3bda.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        // Fungsi untuk menangani pengiriman formulir
        function submitForm(event) {
            // Mencegah perilaku bawaan formulir yang merefresh halaman saat disubmit
            event.preventDefault();

            // Mengambil nilai dari inputan formulir
            var nama = document.getElementById('nama').value;
            var email = document.getElementById('email').value;
            var email = document.getElementById('status').value;
            var email = document.getElementById('telepon').value;
            var pesan = document.getElementById('pesan').value;

            // Menampilkan pesan konfirmasi
            alert('Terima kasih, ' + nama + '! Umpan balik Anda telah dikirim.\n\nEmail: ' + email + '\nPesan: ' + pesan);

            // Reset formulir setelah pengiriman
            document.getElementById('feedbackForm').reset();
        }
    </script> -->

</body>

</html>