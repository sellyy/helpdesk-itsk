@extends('admin.template.main')

@section('title', 'Dashboard Admin - Helpdesk ITSK')

@section('content')
<div class="page-content mt-n4">
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin" id="top-content">
        <div>
            <h4 class="mb-3 mb-md-0" id="welcome">Selamat Datang di Halaman Admin! <img src="../assets/images/wave.gif" id="hand"></h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap" id="bt-group">
            <div class="input-group date datepicker wd-200 me-2 mb-2 mb-md-0" id="dashboardDate">
                <span class="input-group-text input-group-addon bg-transparent border-success"><i data-feather="calendar" class=" text-success"></i></span>
                <input type="text" class="form-control border-success bg-transparent" id="bt-date">
            </div>
            <button type="button" class="btn btn-success btn-icon-text mb-2 mb-md-0 text-light" id="bt-download">
                <i class="btn-icon-prepend" data-feather="download-cloud"></i>
                Download Report
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-4 col-xl-4">
            <div class="card" id="card1">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <h1 class="pb-2">{{ $jumlahBelumDiproses }}</h1>
                        <div class="pb-2"> <i data-feather="x-square"></i> Belum Diproses </div>
                    </div>
                    <span data-peity='{"fill": ["rgb(251,188,6)"],"height": 50, "width": 80 }' class="peity-bar">5,3,9,6,5,9,7,3,5,2</span>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4 col-xl-4">
            <div class="card" id="card2">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <h1 class="pb-2">{{ $jumlahSedangDiproses }}</h1>
                        <div class="pb-2"> <i data-feather="activity"></i> Sedang Diproses </div>
                    </div>
                    <span data-peity='{"fill": ["rgb(251,188,6)"],"height": 50, "width": 80 }' class="peity-bar">5,3,9,6,5,9,7,3,5,2</span>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4 col-xl-4">
            <div class="card" id="card3">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <h1 class="pb-2">{{ $jumlahSelesai }}</h1>
                        <div class="pb-2"> <i data-feather="check"></i> Selesai </div>
                    </div>
                    <span data-peity='{"stroke": ["rgb(251,188,6)"], "fill": ["rgba(251,188,6, .2)"],"height": 50, "width": 80 }' class="peity-line">5,3,9,6,5,9,7,3,5,2</span>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h6 class="card-title">Data Laporan Pengaduan</h6>
                    <div class="table-responsive">
                        <table id="dataTableTiket" class="table hover stripe">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Posisi</th>
                                    <th>No. Tiket</th>
                                    <th>Email</th>
                                    <th>No. Telp</th>
                                    <th>Prodi</th>
                                    <th>Unit Kerja</th>
                                    <th>NIP</th>
                                    <th>NIM</th>
                                    <th>NIK</th>
                                    <th>Judul Tiket</th>
                                    <th>Urgensi</th>
                                    <th>Kategori</th>
                                    <th>Kategori (Jika Memilih 'Lainnya')</th>
                                    <th>Deskripsi</th>
                                    <th>Waktu</th>
                                    <th>Lokasi</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1 @endphp
                                @foreach ($tikets as $tiket)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $tiket->nama }}</td>
                                    <td>{{ $tiket->posisi }}</td>
                                    <td>{{ $tiket->no_tiket }}</td>
                                    <td>{{ $tiket->email }}</td>
                                    <td>{{ $tiket->no_telepon }}</td>
                                    <td>{{ $tiket->prodi }}</td>
                                    <td>{{ $tiket->unit_kerja_karyawan }}</td>
                                    <td>{{ $tiket->nip_dosen }}</td>
                                    <td>{{ $tiket->nim_mahasiswa }}</td>
                                    <td>{{ $tiket->nik_karyawan }}</td>
                                    <td>{{ $tiket->judul_tiket }}</td>
                                    <td>{!! $tiket->tingkat_urgensi !!}</td>
                                    <td>{{ $tiket->kategori_laporan }}</td>
                                    <td>{{ $tiket->kategori_lainnya }}</td>
                                    <td>{{ $tiket->deskripsi_tiket }}</td>
                                    <td>{{ $tiket->waktu_tiket }}</td>
                                    <td>{{ $tiket->lokasi_tiket }}</td>
                                    <td>{!! $tiket->status_tiket !!}</td>
                                    <td>Aksi</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    document.getElementById('bt-download').addEventListener('click', function() {
        var selectedDate = document.getElementById('bt-date').value;
        window.location.href = '/generate-excel?date=' + encodeURIComponent(selectedDate);
    });
</script>
@endpush

@push('style')
<style>
    #hand {
        width: 35px;
        margin-top: -10px;
    }

    #dataTableTiket th:nth-child(2),
    #dataTableTiket td:nth-child(2) {
        position: sticky;
        left: 0;
        background-color: white;
        z-index: 1;
    }

    @media only screen and (max-width: 1095px) {
        #bt-group {
            margin-top: 10px;

        }

        #top-content {
            flex-direction: column;
        }
    }

    @media only screen and (max-width: 768px) {
        #bt-group {
            margin-top: 0;
        }

        #card2 {
            margin-top: 10px;
        }

        #card3 {
            margin-top: 10px;
        }
    }

    @media only screen and (max-width: 476px) {
        #bt-group {
            margin-top: 0;
            flex-direction: column;
        }

        #welcome {
            font-size: 17px;
        }

        #bt-download {
            display: block;
            width: 100%;
        }
    }

    @media only screen and (max-width: 423px) {

        #welcome {
            font-size: 15px;
        }

        #hand {
            width: 25px;
        }
    }
</style>
@endpush