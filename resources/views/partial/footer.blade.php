<footer class="z-3">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <a class="navbar-brand primary-bg fs-6 p-2 rounded-4 text-white poppins-regular"
                   href="#">ITSK Soepraoen</a>
                <p class="fs-6 mt-4 urbanist fw-medium">Jl. S. Supriadi No.22, Sukun, Kec. Sukun, Kota Malang,
                    Jawa Timur Kode Pos:
                    65147
                </p>
            </div>
            <div class="col-lg-2 offset-lg-3">
                <h5 class="urbanist fw-bold">Layanan</h5>
                <ul class="list-unstyled text-small lh-lg">
                    <li>
                        <a class="nav-link urbanist fw-normal" href="/#panduan">Panduan</a>
                    </li>
                    <li>
                        <a class="nav-link urbanist fw-normal" href="/#statusTiket">Status Tiket</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3">
                <h5 class="urbanist fw-bold">Contact</h5>
                <div class="d-flex gap-3">
                    <a href="#" class="nav-link"><i
                            class="fa-brands fa-facebook-f sosial-media text-white p-3"></i></a>
                    <a href="#" class="nav-link"><i
                            class="fa-brands sosial-media text-white p-3 fa-instagram"></i></a>
                    <a href="" class="nav-link"><i
                            class="fa-brands sosial-media text-white p-3 fa-youtube"></i></a>
                </div>
            </div>
        </div>
        <hr class="mt-5">
        <div class="row text-center">
            <p class="fs-6 urbanist fw-normal">© Copyright - Institut Teknologi Sains & Kesehatan dr. Soepraon 2024
            </p>
        </div>
    </div>
</footer>
