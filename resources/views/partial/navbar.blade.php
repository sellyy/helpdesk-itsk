{{-- Navbar --}}
<div class="row">
    <div class="col-md-8 offset-md-2">

        <nav class="navbar navbar-expand-lg bg-white mt-3 rounded-4 z-1 shadow">
            <div class="container text-center">
                <a class="navbar-brand primary-bg fs-6 p-2 rounded-4 text-white poppins-regular" href="/">ITSK
                    Soepraoen</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-center justify-content-lg-around" id="navbarNav">

                    <ul class="navbar-nav gap-lg-5 align-items-center mx-lg-3">
                        <li class="nav-item">
                            <a class="nav-link fs-5 fw-light active" aria-current="page" href="/#panduan">Panduan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/#faq">FAQ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/#statusTiket">Status Tiket</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/umpan-balik">Umpan Balik</a>
                        </li>
                    </ul>
                    <a href="nav-link">
                        <img src="assets/images/konsultasi.png" alt="">
                    </a>

                </div>
            </div>
        </nav>
    </div>
</div>
{{-- End Navbar --}}