<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    @vite(['resources/css/app.scss', 'resources/js/app.js'])
    <title>Lapor Mahasiswa</title>
</head>

<body>
    <div class="container-fluid">

        @include('partial.navbar')

        <div class="container mt-4">

            {{-- Breadcrumb --}}
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/" class="text-decoration-none text-primary-emphasis">Beranda</a>
                    </li>
                    <li class="breadcrumb-item"><a href="/" class="text-decoration-none text-primary-emphasis">Pelaporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Mahasiswa</li>
                </ol>
            </nav>
            {{-- End Breadcrumb --}}

            {{-- Tabs Form --}}
            <ul class="nav justify-content-around d-flex align-items-center" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active text-secondary active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Data Diri
                    </button>
                </li>
                <li class="nav-item">
                    <i class="fa-solid fa-angles-right text-secondary"></i>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link text-secondary" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Masalah
                    </button>
                </li>
                <li class="nav-item">
                    <i class="fa-solid fa-angles-right text-secondary"></i>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link text-secondary" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact-tab-pane" type="button" role="tab" aria-controls="contact-tab-pane" aria-selected="false">Selesai
                    </button>
                </li>
            </ul>
            <hr class="border border-success">
            {{-- End Tabs Form --}}

            {{-- Form Pelaporan --}}
            <form action="" method="POST" id="myForm">
                @csrf
                <div class="tab-content" id="myTabContent">

                    {{-- Form Data Diri --}}
                    <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                        <div class="row d-flex justify-content-between">
                            <div class="col-lg-3 mt-lg-5 mt-3">
                                <div class="mb-3">
                                    <input type="text" class="form-control rounded-1 py-2 px-3 border border-success-subtle mb-3 mb-lg-5" name="namalengkap" id="namalengkap" aria-describedby="helpId" placeholder="Nama Lengkap" />

                                    <input type="text" class="form-control rounded-1 py-2 px-3 border border-success-subtle mb-3 mb-lg-5" name="nim" id="nim" aria-describedby="helpId" placeholder="Nomor Induk Mahasiswa" />
                                </div>
                            </div>
                            <div class="col-lg-3 mt-lg-5 mt-3">
                                <input type="email" class="form-control rounded-1 py-2 px-3 border border-success-subtle mb-3 mb-lg-5" name="email" id="email" aria-describedby="helpId" placeholder="Alamat Email" />
                                <select class="form-select form-select border border-success-subtle mb-3 mb-lg-5" name="prodi" id="prodi">
                                    <option hidden>Prodi</option>
                                    <option value="Informatika">Informatika</option>
                                    <option value="Sistem Informasi">Sistem Informasi</option>
                                    <option value="Jaringan Komputer">Jaringan Komputer</option>
                                </select>
                            </div>
                            <div class="col-lg-3 mt-lg-5 mt-3">
                                <input type="number" class="form-control rounded-1 py-2 px-3 border border-success-subtle mb-3 mb-lg-5" name="telp" id="telp" aria-describedby="helpId" placeholder="Nomor Telepon" />
                                <input type="number" class="form-control rounded-1 py-2 px-3 border border-success-subtle mb-3 mb-lg-5" name="semester" id="semester" aria-describedby="helpId" placeholder="Semester" />
                            </div>
                        </div>
                        <hr class="border border-success-subtle">
                        <section class="container d-lg-flex justify-content-between">
                            <div class="col-lg-8">
                                <p><span class="text-danger">*</span>Pastikan semua data yang Anda masukkan sudah benar
                                    dan
                                    lengkap. Kesalahan dalam
                                    pengisian data dapat menghambat proses selanjutnya. Tekan <span class="fw-semibold">"Selanjutnya"</span>untuk menuju
                                    langkah berikutnya.</p>
                            </div>
                            <div class="col-lg-3">
                                <button class="btn w-100 rounded-0 text-white py-2" id="goto-masalah" type="button" style="background-color: #81D742">Selanjutnya
                                </button>
                            </div>

                        </section>
                    </div>
                    {{-- End Form Data Diri --}}

                    {{-- Form Masalah --}}
                    <div class="tab-pane fade my-3" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                        <p class="fs-5">Tuliskan Masalah yang ingin kami bantu</p>
                        <div class="row">
                            <div class="col-lg-6">
                                <p>Subjek</p>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control rounded-1 py-2 w-lg-50 px-3 border border-success-subtle mb-4" name="subjek" id="subjek" aria-describedby="helpId" placeholder="Tulis disini" />
                                    </div>
                                </div>
                                <p>Tingkat urgensi bantuan menggambarkan tingkat kepentingan seberapa cepat aduan
                                    tersebut perlu diatasi.</p>
                                <div class="d-flex justify-content-between gap-2 mb-4">
                                    <input type="radio" name="urgensi" class="btn-check" id="btn-check-outlined" value="rendah" autocomplete="off">
                                    <label class="btn btn-outline-success fs-6 px-3 text-start" for="btn-check-outlined">Rendah<i class="fa-regular fa-circle-check ms-1"></i></label>
                                    <input type="radio" name="urgensi" class="btn-check" id="btn-check-outlined2" value="sedang" autocomplete="off">
                                    <label class="btn btn-outline-success fs-6 px-3 text-start" for="btn-check-outlined2">Sedang<i class="fa-regular fa-circle-check ms-1"></i></label>
                                    <input type="radio" name="urgensi" class="btn-check" id="btn-check-outlined3" value="Tinggi" autocomplete="off">
                                    <label class="btn btn-outline-success fs-6 px-3" for="btn-check-outlined3">
                                        Tinggi
                                        <i class="fa-regular fa-circle-check ms-1"></i>
                                    </label>
                                </div>

                                <p>Kategori laporan untuk pengelompokkan masalah atau pertanyaan agar lebih mudah
                                    dikelola dan dialokasikan ke bagian penanganan masalah.</p>
                                <div class="d-flex justify-content-lg-between justify-content-evenly gap-2 flex-wrap">
                                    <input type="radio" name="kategori" class="btn-check" id="teknis" value="teknis" autocomplete="off">
                                    <label class="btn btn-outline-success fs-6 px-3 text-start" for="teknis">Teknis<i class="fa-regular fa-circle-check ms-1"></i></label>
                                    <input type="radio" name="kategori" class="btn-check" id="administratif" value="administratif" autocomplete="off">
                                    <label class="btn btn-outline-success fs-6 px-3 text-start" for="administratif">Administratif<i class="fa-regular fa-circle-check ms-1"></i></label>
                                    <input type="radio" name="kategori" class="btn-check" id="akademis" value="akademis" autocomplete="off">
                                    <label class="btn btn-outline-success fs-6 px-3" for="akademis">
                                        Akademis
                                        <i class="fa-regular fa-circle-check ms-1"></i>
                                    </label>
                                    <input type="radio" name="kategori" class="btn-check" id="lainnya" value="lainnya" autocomplete="off">
                                    <label class="btn btn-outline-success fs-6 px-3" for="lainnya">
                                        Lainnya
                                        <i class="fa-regular fa-circle-check ms-1"></i>
                                    </label>
                                </div>
                                <input type="text" class="form-control rounded-1 mt-3 py-2 w-lg-50 px-3 border border-success-subtle mb-3 mb-lg-5" name="kategori-lain" id="kategory" aria-describedby="helpId" placeholder="Tulis disini" disabled />
                            </div>

                            <div class="col-lg-4 offset-lg-2">
                                <p>Deskripsi Aduan</p>
                                <div class="form-floating">
                                    <textarea class="form-control" placeholder="Tulis disini" id="deskripsi" name="deskripsi" style="height: 100px"></textarea>
                                    <label for="deskripsi">Tulis disini</label>
                                </div>
                                <div class="my-3">
                                    <label for="formFile" class="form-label">Tambah Berkas</label>
                                    <input class="form-control" name="formFile" type="file" id="formFile">
                                </div>
                                <div class="card p-3 gap-3">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <label for="waktu" class="form-label ms-0"><i class="fa-regular me-2 fa-clock"></i>Jam</label>
                                        <input class="form-control w-50" type="time" id="waktu" name="waktu" value="00:00" min="00:00" max="12:00">
                                    </div>
                                    <div class="d-flex align-items-center justify-content-between">
                                        <label for="tanggal" class="form-label"><i class="fa-regular me-2 fa-calendar"></i>Tanggal</label>
                                        <input class="form-control w-50" type="date" id="tanggal" name="tanggal">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <hr class="border border-success-subtle">
                        <section class="container d-lg-flex justify-content-between">
                            <div class="col-lg-8">
                                <p><span class="text-danger">*</span>Pastikan semua data yang Anda masukkan sudah benar
                                    dan
                                    lengkap. Kesalahan dalam
                                    pengisian data dapat menghambat proses selanjutnya. Tekan <span class="fw-semibold">"Selanjutnya"</span>untuk menuju
                                    langkah berikutnya.</p>
                            </div>
                            <div class="col-lg-3">
                                <button type="button" onclick="submitForm()" class="btn w-100 rounded-0 text-white py-2" id="goto-selesai" style="background-color: #81D742">Selanjutnya
                                </button>
                            </div>

                        </section>
                    </div>
                    {{-- End Form Masalah --}}

                    {{-- Form Selesai --}}
                    <div class="tab-pane fade" id="contact-tab-pane" role="tabpanel" aria-labelledby="contact-tab" tabindex="0">
                        <p>Kami ingin mengucapkan terima kasih yang sebesar-besarnya atas laporan masalah yang Anda
                            kirimkan. Kami memahami bahwa Anda mengalami kendala dan kami di sini untuk membantu
                            menyelesaikannya.</p>
                        <div class="primary-bg col-lg-3 mx-auto col-md-6 text-center rounded-3">
                            <h1 class="text-white p-4 rounded-3 my-5 fs-4">Code :
                                ITSKDRSA002171
                            </h1>
                        </div>
                        <p>Laporan Anda telah ditambahkan ke antrian dan kami akan segera menindaklanjutinya. Anda akan
                            menerima email konfirmasi dengan nomor tiket Anda. Simpan nomor ini untuk referensi di masa
                            depan. Tim kami akan meninjau laporan Anda dan berusaha menyelesaikan masalah secepat
                            mungkin.
                            Kami akan terus mengabarkan Anda melalui email tentang perkembangannya. </p>
                        <p>Jika Anda memiliki pertanyaan atau membutuhkan bantuan lebih lanjut, jangan ragu untuk
                            menghubungi kami. Anda dapat membalas email konfirmasi atau mengunjungi situs web kami untuk
                            informasi lebih lanjut. Sekali lagi, terima kasih atas kesabaran dan pengertian Anda. Kami
                            berkomitmen untuk memberikan layanan terbaik dan menyelesaikan masalah Anda sesegera
                            mungkin.
                        </p>
                        <hr class="border border-success-subtle">
                        <section class="container d-lg-flex justify-content-between">
                            <div class="col-lg-8">
                                <p><span class="text-danger">*</span>Pastikan mengecek status antrian dalam kurun waktu 5
                                    hari sekali. Klik Tutup untuk menuju halaman untama..</p>
                            </div>
                            <div class="col-lg-3">
                                <button class="btn w-100 rounded-0 text-white py-2" id="goto-home" type="button" style="background-color: #81D742"><a href="/" class="nav-link">Tutup</a>
                                </button>
                            </div>

                        </section>
                    </div>
                    {{-- Form Selesai --}}
                </div>

                {{-- End Form Pelaporan --}}


        </div>
        </form>

    </div>

    </div>

    <script src="https://kit.fontawesome.com/c3621d3bda.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        /**
         * Script untuk mengatur perilaku form kategori pelaporan.
         */

        const lainnya = document.querySelector('#lainnya');
        const kategoriLain = document.querySelector('#kategory');

        // Menambahkan event listener untuk saat elemen "lainnya" diklik
        lainnya.addEventListener('click', () => {
            // Mengaktifkan input "kategory" jika elemen "lainnya" diklik
            kategoriLain.disabled = false;
        });

        // Mendapatkan referensi semua elemen radio dengan name "kategori"
        const kategoriRadios = document.querySelectorAll('input[name="kategori"]');

        // Menambahkan event listener untuk setiap elemen radio "kategori"
        kategoriRadios.forEach(radio => {
            radio.addEventListener('click', () => {
                // Menonaktifkan input "kategory" jika elemen radio selain "lainnya" diklik
                if (radio !== lainnya) {
                    kategoriLain.disabled = true;
                }
            });
        });

        const goToMasalah = document.querySelector('#goto-masalah');
        const goToSelesai = document.querySelector('#goto-selesai');

        goToMasalah.addEventListener('click', () => {
            document.querySelector('#home-tab-pane').classList.remove('active');
            document.querySelector('#home-tab-pane').classList.remove('show');
            document.querySelector('#profile-tab-pane').classList.add('active');
            document.querySelector('#profile-tab-pane').classList.add('show');
        });

        goToSelesai.addEventListener('click', () => {
            document.querySelector('#profile-tab-pane').classList.remove('active');
            document.querySelector('#profile-tab-pane').classList.remove('show');
            document.querySelector('#contact-tab-pane').classList.add('active');
            document.querySelector('#contact-tab-pane').classList.add('show');
        });


        function submitForm() {
            var form = document.getElementById('myForm');
            var formData = new FormData(form);

            fetch('/lapor-{role}', {
                    method: 'POST',
                    body: formData
                })
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Gagal melakukan submit form');
                    }
                })
                .then(data => {
                    //console.log('Tiket berhasil disimpan:', data);
                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: "Laporan Berhasil 🤞",
                        showConfirmButton: false,
                        timer: 1700
                    });
                })
                .catch(error => {
                    console.error('Terjadi kesalahan:', error);
                });
        }
    </script>

</body>

</html>