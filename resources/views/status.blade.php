<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/timeline.css') }}">
    @vite(['resources/css/app.scss', 'resources/js/app.js'])
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200"/>
    <title>Status Tiket</title>
</head>
<body>
<section class="container-fluid">
    @include('partial.navbar')
    <main class="row my-5">
        <div class="col-md-6 offset-md-3 shadow rounded-4" style="height: 70vh">
            <h4 class="fw-bold text-center urbanist mt-5">STATUS TIKET PENGADUAN</h4>
            <div class="d-flex justify-content-center">
                <div class="col-xs-10 col-xs-offset-2 col-sm-8 col-sm-offset-2 offset-md-1 mt-5">
                    <ul class="timeline">
                        <li class="timeline-item">
                            <div class="timeline-info">
                                <span>Laporan dikirim ke teknisi</span>
                            </div>
                            <div class="timeline-marker"></div>
                            <div class="timeline-content">
                                <p>Estimasi : Rabu 06 Maret 2024, 10.30 WIB </p>
                            </div>
                        </li>
                        <li class="timeline-item">
                            <div class="timeline-info">
                                <span>Laporan sedang diperiksa admin</span>
                            </div>
                            <div class="timeline-marker"></div>
                            <div class="timeline-content">
                                <p>Estimasi : Kamis 07 Maret 2024, 10.30 WIB </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </main>
</section>
@include('partial.footer')

<script>
    try {
        f
        Typekit.load({
            async: true
        });
    } catch (e) {
    }
</script>
<script src="https://use.typekit.net/bkt6ydm.js"></script>
<script src="https://kit.fontawesome.com/c3621d3bda.js" crossorigin="anonymous"></script>
</body>
</html>
