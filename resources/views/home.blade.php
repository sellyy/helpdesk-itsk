<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200"/>
    <title>Helpdesk - ITSK</title>
    @vite(['resources/css/app.scss', 'resources/js/app.js'])
</head>

<body>
{{-- Hero Section --}}
<div class="container-fluid min-vh-100 heroSection">

    @include('partial.navbar')

    <div class="row py-5" style="height: 90vh">
        <div class="col-lg-4 col-md-8 offset-md-2">
            <div class="h-100 w-100 d-flex flex-column justify-content-center">
                <h1 class="urbanist-bold">Apakah <br> Butuh Bantuan ?</h1>
                <p class="">
                    Anda mengalami masalah yang bertentangan dengan TI? Klik disini
                    untuk melaporkan masalah yang anda hadapi !
                </p>
                <div class="dropdown">
                    <button
                        class="urbanist-semibold dropdown-button bols-outlined d-flex align-items-center shadow-sm"
                        type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Laporkan Masalah Sekarang
                        <span class="material-symbols-outlined ms-4">expand_circle_down</span>
                    </button>
                    <ul class="dropdown-menu w-50 shadow">
                        <li>
                            <a class="dropdown-item urbanist" href="/lapor-dosen">
                                <i class="me-1 fa-regular fa-circle-user"></i>
                                Dosen
                                <i class="fa-solid fa-chevron-right float-end"></i>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item urbanist" href="/lapor-mahasiswa">
                                <i class="me-1 fa-regular fa-circle-user"></i>
                                Mahasiswa
                                <i class="fa-solid fa-chevron-right float-end"></i>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item urbanist" href="/lapor-karyawan">
                                <i class="me-1 fa-regular fa-circle-user"></i>
                                Karyawan
                                <i class="fa-solid fa-chevron-right float-end"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-9 offset-md-2 offset-lg-0 ">
            <img id="heroImageContent" class="img-fluid z-2" src="assets/images/hero-image.png" alt="">
        </div>
    </div>
</div>

{{-- Panduan Section --}}
<section id="panduan" class="container-fluid min-vh-100 my-lg-5 my-lg-0">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 d-flex flex-column justify-content-center align-items-center">
                <h2 class="urbanist-bold">Panduan</h2>
                <p class="fs-3 urbanist fw-medium">Pengaduan Helpdesk</p>
            </div>
        </div>
        <div class="row row-gap-5 px-3">
            <div class="row d-flex justify-content-center gap-3">
                <div class="col-lg-3">
                    <div class="card p-4 h-100">
                        <div class="card-body urbanist fw-light position-relative">
                            <h1 class="fs-3 text-center primary-bg w-10 p-2 rounded-3 no-panduan">01</h1>
                            <h5 class="card-title urbanist fw-semibold">
                                Tuliskan masalah anda
                                di Form Pengaduan
                            </h5>
                            Untuk mengisi form pengaduan klik tombol hijau di atas “Temukan Solusi Masalah Anda
                            Sekarang!”.
                        </div>
                    </div>
                </div>
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <i class="fa-solid fa-arrow-right d-none d-lg-block"></i>
                </div>
                <div class="col-lg-3">
                    <div class="card p-4 h-100">
                        <div class="card-body urbanist fw-light position-relative">
                            <h1 class="fs-3 text-center primary-bg w-10 p-2 rounded-3 no-panduan">02</h1>
                            <h5 class="card-title urbanist fw-semibold">
                                Lengkapi data Pada Form Pengaduan
                            </h5>
                            Anda diperlukan untuk melengkapi data pada form pengaduan untuk melaporkan masalah anda.
                        </div>
                    </div>
                </div>
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <i class="fa-solid fa-arrow-right d-none d-lg-block"></i>
                </div>
                <div class="col-lg-3">
                    <div class="card p-4 h-100">
                        <div class="card-body urbanist fw-light position-relative">
                            <h1 class="fs-3 text-center primary-bg w-10 p-2 rounded-3 no-panduan">03</h1>
                            <h5 class="card-title urbanist fw-semibold">
                                Check Kembali Aduan Anda
                            </h5>
                            Anda perlu mengecheck kembali form pengaduan untuk memastikan bahwa pengaduan anda
                            benar.
                        </div>
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center align-items-center gap-3 flex-column-reverse flex-lg-row">
                <div class="col-lg-3">
                    <div class="card p-4 h-100">
                        <div class="card-body urbanist fw-light position-relative">
                            <h1 class="fs-3 text-center primary-bg w-10 p-2 rounded-3 no-panduan">06</h1>
                            <h5 class="card-title urbanist fw-semibold">
                                Check Status Tiket Anda
                            </h5>
                            Untuk mengetahui proses pengaduan, anda dapat mengechecknya pada menu “Status Tiket”
                            dengan memasukkan kode tiket anda.
                        </div>
                    </div>
                </div>
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <i class="fa-solid fa-arrow-left d-none d-lg-block"></i>
                </div>
                <div class="col-lg-3">
                    <div class="card p-4 h-100">
                        <div class="card-body urbanist fw-light position-relative">
                            <h1 class="fs-3 text-center primary-bg w-10 p-2 rounded-3 no-panduan">05</h1>
                            <h5 class="card-title urbanist fw-semibold">
                                Proses Pengecheckkan Pengaduan
                            </h5>
                            Pengaduan akan di check oleh admin untuk di proses lebih lanjut.
                        </div>
                    </div>
                </div>
                <div class="col-1 d-flex align-items-center justify-content-center">
                    <i class="fa-solid fa-arrow-left d-none d-lg-block"></i>
                </div>
                <div class="col-lg-3">
                    <div class="card p-4 h-100">
                        <div class="card-body urbanist fw-light position-relative">
                            <h1 class="fs-3 text-center primary-bg w-10 p-2 rounded-3 no-panduan">04</h1>
                            <h5 class="card-title urbanist fw-semibold">
                                Simpan Kode Tiket Anda
                            </h5>
                            Simpan kode tiket anda untuk mengetahui seberapa jauh masalah anda di proses.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

{{-- FAQ Section --}}
<section id="faq" class="container-fluid min-vh-100 d-flex justify-content-center align-items-center">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12 offset-md-2 d-flex flex-column align-items-center">
                <h2 class="urbanist fw-semibold">Frequently Asked Question</h2>
                <p class="fs-3 primary-txt urbanist fw-semibold">Helpdesk</p>
            </div>
            <div class="col-lg-4 offset-lg-4">
                <input type="text"
                       class="urbanist fw-normal text-center form-control border border-success-subtle my-lg-5 my-3"
                       placeholder="Ketik kata kunci dari kendala anda"
                       aria-label="Ketik kata kunci dari kendala anda . . .">
            </div>
        </div>

        <div class="row d-flex justify-content-center mt-lg-5">
            <div class="col-lg-7 shadow-lg border border-success-subtle">
                <div class="accordion accordion-flush" id="accordionFlushExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button urbanist fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseOne"
                                    aria-expanded="false" aria-controls="flush-collapseOne">
                                Bagaimana Cara melihat Kemajuan Proses Laporan Pengaduan?
                            </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse show"
                             data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body urbanist fs-6 fw-medium">Anda dapat mengechecknya dengan
                                pergi ke menu
                                “Status Tiket”, lalu masukkan kode tiket yang telah anda dapatkan setelah mengisi
                                form pengaduan atau anda juga bisa dapat melihat kode tiket dari email yang telah di
                                kirimkan oleh sistem.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button urbanist fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo"
                                    aria-expanded="false" aria-controls="flush-collapseTwo">
                                Bagaimana jika saya menghilangkan atau lupa kode tiketnya?
                            </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse"
                             data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body urbanist fs-6 fw-medium">Placeholder content for this
                                accordion, which
                                is intended to demonstrate the <code>.accordion-flush</code> class. This is the
                                second item's accordion body. Let's imagine this being filled with some actual
                                content.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button urbanist fw-semibold collapsed" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseThree"
                                    aria-expanded="false" aria-controls="flush-collapseThree">
                                Mengapa Kode tiket help desk harus di simpan?
                            </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse collapse"
                             data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body urbanist fs-6 fw-medium">Placeholder content for this
                                accordion, which
                                is intended to demonstrate the <code>.accordion-flush</code> class. This is the
                                third item's accordion body. Nothing more exciting happening here in terms of
                                content, but just filling up the space to make it look, at least at first glance, a
                                bit more representative of how this would look in a real-world application.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- Status Ticket Section --}}
<section id="statusTiket" class="container-fluid pt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12 offset-md-2 d-flex flex-column align-items-center">
                <h2 class="urbanist fw-bold">Status Tiket Pengaduan</h2>
                <p class="fs-6 fw-light">Periksa status tiket anda secara berkala di bawah ini !</p>
            </div>
            <div class="col-lg-4 offset-lg-4 text-center">
                <input type="text"
                       class="urbanist text-center form-control border border-success-subtle my-lg-2 my-3"
                       placeholder="Masukkan kode Tiket Anda" aria-label="Masukkan kode Tiket Anda">
                <button type="submit" class="cek-tiket text-center urbanist fw-semibold">Submit</button>
            </div>
        </div>
    </div>
    <div class="w-full h-full">
        <img src="assets/images/status-ticket-bg-section.webp.png" id="image-status" class="img-fluid"
             alt="">
    </div>
</section>

{{-- Footer --}}
<footer class="z-3">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <a class="navbar-brand primary-bg fs-6 p-2 rounded-4 text-white poppins-regular"
                   href="#">ITSK Soepraoen</a>
                <p class="fs-6 mt-4 urbanist fw-medium">Jl. S. Supriadi No.22, Sukun, Kec. Sukun, Kota Malang,
                    Jawa Timur Kode Pos:
                    65147
                </p>
            </div>
            <div class="col-lg-2 offset-lg-3">
                <h5 class="urbanist fw-bold">Layanan</h5>
                <ul class="list-unstyled text-small lh-lg">
                    <li>
                        <a class="nav-link urbanist fw-normal" href="#panduan">Panduan</a>
                    </li>
                    <li>
                        <a class="nav-link urbanist fw-normal" href="$statusTiket">Status Tiket</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3">
                <h5 class="urbanist fw-bold">Contact</h5>
                <div class="d-flex gap-3">
                    <a href="#" class="nav-link"><i
                            class="fa-brands fa-facebook-f sosial-media text-white p-3"></i></a>
                    <a href="#" class="nav-link"><i
                            class="fa-brands sosial-media text-white p-3 fa-instagram"></i></a>
                    <a href="" class="nav-link"><i
                            class="fa-brands sosial-media text-white p-3 fa-youtube"></i></a>
                </div>
            </div>
        </div>
        <hr class="mt-5">
        <div class="row text-center">
            <p class="fs-6 urbanist fw-normal">© Copyright - Institut Teknologi Sains & Kesehatan dr. Soepraon 2024
            </p>
        </div>
    </div>
</footer>

<script src="https://kit.fontawesome.com/c3621d3bda.js" crossorigin="anonymous"></script>

</body>

</html>
