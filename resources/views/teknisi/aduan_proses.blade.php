@extends('teknisi.template.main')

@section('title', 'Data Aduan Sedang Diproses - Helpdesk ITSK')

@section('content')
<div class="page-content mt-n4">
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin" id="top-content">
        <h4>Data Aduan Sedang Diproses</h4>
        <div class="d-flex align-items-center flex-wrap text-nowrap" id="bt-group">
            <div class="input-group date datepicker wd-200 me-2 mb-2 mb-md-0" id="dashboardDate">
                <span class="input-group-text input-group-addon bg-transparent border-success"><i data-feather="calendar" class=" text-success"></i></span>
                <input type="text" class="form-control border-success bg-transparent" id="bt-date">
            </div>
            <button type="button" class="btn btn-success btn-icon-text mb-2 mb-md-0 text-light" id="bt-download">
                <i class="btn-icon-prepend" data-feather="download-cloud"></i>
                Download Report
            </button>
        </div>
    </div>

    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-4" id="top-content">
                            <h6 class="card-title m-0">Data Tiket Pengaduan</h6>
                        </div>
                        <div class="table-responsive">
                            <table id="dataTableExample" class="table hover stripe">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Kategori Masalah</th>
                                        <th>Deskripsi Masalah</th>
                                        <th>Tanggal Masuk</th>
                                        <th>Estimasi Selesai</th>
                                        <th>Tanggapan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1 @endphp
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>Gojo Satoru</td>
                                        <td>Karyawan</td>
                                        <td>WiFi Tidak Stabil</td>
                                        <td>Dapat terhubung namun tidak memiliki koneksi internet</td>
                                        <td>23/11/2024</td>
                                        <td>23/11/2024</td>
                                        <td><button class="btn btn-success" onclick="selesaiButton(this)">Selesai</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection