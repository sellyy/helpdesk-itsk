@extends('superadmin.template.main')

@section('title', 'Profil - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="page-content mt-n4">
            <div class="row">
                <div class="col-md-6 grid-margin stretch-card my-auto">
                    <img src="../assets/images/profil.png" alt="img" class="img-fluid">
                </div>
                <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title">Informasi Pribadi</h6>
                            <p class="text-muted pb-2">Mengelola informasi pribadi Anda.</p>
                            <form class="forms-sample">
                                <div class="mb-3">
                                    <label for="exampleInputUsername1" class="form-label">Username</label>
                                    <input type="text" class="form-control" id="exampleInputUsername1" autocomplete="off"
                                        placeholder="Username">
                                </div>
                                <button type="submit" class="btn btn-success me-2">Submit</button>
                                <button class="btn btn-secondary">Cancel</button>
                            </form>
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">Kata Sandi</h6>
                            <p class="text-muted pb-2">Minimal harus terdiri dari 8 karakter.</p>
                            <form class="forms-sample">
                                <div class="mb-3">
                                    <label for="exampleInputPassword1" class="form-label">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1"
                                        autocomplete="off" placeholder="Password">
                                </div>
                                <div class="mb-3">
                                    <label for="exampleInputPassword1" class="form-label">Konfirmasi Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1"
                                        autocomplete="off" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-success me-2">Submit</button>
                                <button class="btn btn-secondary">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('style')
    <style>
        .img-fluid {
            max-width: 90%;
            max-height: 80%;
        }
        @media only screen and (max-width: 775px) {
            .img-fluid {
                display: none;
            }
        }
    </style>
@endpush