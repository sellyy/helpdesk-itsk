@extends('superadmin.template.main')

@section('title', 'Data Pengguna - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-4" id="top-content">
                            <h6 class="card-title m-0">Data Pengguna</h6>
                            <div class="bt-group">
                                <button type="button" id="bt-tambah" class="btn btn-success btn-sm btn-icon-text"><i
                                        class="link-icon" data-feather="plus-square"></i> Tambah Data</button>
                                <button type="button" class="btn btn-danger btn-sm btn-icon-text" id="bt-del"><i
                                        class="link-icon" data-feather="x-square"></i> Hapus Data</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="dataTableExample" class="table hover stripe">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" class="form-check-input check-all"></th>
                                        <th>Nama Pengguna</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="checkbox" class="form-check-input check"></td>
                                        <td>John Bayer</td>
                                        <td>jbayer@mail.com</td>
                                        <td><span class="badge bg-primary">Super Admin</span></td>
                                        <td><i class="link-icon" data-feather="check-circle" id="online"></i></td>
                                        <td><button type="button" class="btn btn-secondary btn-sm btn-icon-text"><i
                                                    class="link-icon" data-feather="eye"></i> </button>
                                            <button type="button" class="btn btn-success btn-sm btn-icon-text"><i
                                                    class="link-icon" data-feather="edit"></i> </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" class="form-check-input check"></td>
                                        <td>Heru Kahn</td>
                                        <td>herukahn@mail.com</td>
                                        <td><span class="badge bg-info">Teknisi</span></td>
                                        <td><i class="link-icon" data-feather="check-circle" id="online"></i></td>
                                        <td><button type="button" class="btn btn-secondary btn-sm btn-icon-text"><i
                                                    class="link-icon" data-feather="eye"></i> </button>
                                            <button type="button" class="btn btn-success btn-sm btn-icon-text"><i
                                                    class="link-icon" data-feather="edit"></i> </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" class="form-check-input check"></td>
                                        <td>Jane Kimberly</td>
                                        <td>janeeer@mail.com</td>
                                        <td><span class="badge bg-secondary">Admin</span></td>
                                        <td><i class="link-icon" data-feather="check-circle" id="offline"></i></td>
                                        <td><button type="button" class="btn btn-secondary btn-sm btn-icon-text"><i
                                                    class="link-icon" data-feather="eye"></i> </button>
                                            <button type="button" class="btn btn-success btn-sm btn-icon-text"><i
                                                    class="link-icon" data-feather="edit"></i> </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" class="form-check-input check"></td>
                                        <td>Fredo Dredd</td>
                                        <td>fredodredd@mail.com</td>
                                        <td><span class="badge bg-info">Teknisi</span></td>
                                        <td><i class="link-icon" data-feather="check-circle" id="offline"></i></td>
                                        <td><button type="button" class="btn btn-secondary btn-sm btn-icon-text"><i
                                                    class="link-icon" data-feather="eye"></i> </button>
                                            <button type="button" class="btn btn-success btn-sm btn-icon-text"><i
                                                    class="link-icon" data-feather="edit"></i> </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        const checkAllCheckbox = document.querySelector('.check-all');
        const checkboxes = document.querySelectorAll('.check');

        checkAllCheckbox.addEventListener('change', function() {
            checkboxes.forEach(function(checkbox) {
                checkbox.checked = checkAllCheckbox.checked;
            });
        });
    </script>
@endpush

@push('style')
    <style>
        .btn-secondary {
            margin-right: 5px;
        }

        .link-icon {
            max-width: 20px;
        }

        #bt-tambah {
            margin-right: 10px;
        }

        #online {
            color: greenyellow;
        }

        #offline {
            color: gray;
        }

        #dataTableExample th:nth-child(1),
        #dataTableExample td:nth-child(1) {
            position: sticky;
            left: 0;
            background-color: white;
            z-index: 2;
        }

        #dataTableExample th:nth-child(2),
        #dataTableExample td:nth-child(2) {
            position: sticky;
            left: 55px;
            background-color: white;
            z-index: 1;
        }

        @media only screen and (max-width: 556px) {
            #top-content {
                flex-direction: column;
            }

            .bt-group {
                flex-direction: column;
            }

            #bt-tambah {
                width: 100%;
                margin-top: 10px;
            }

            #bt-del {
                width: 100%;
                margin-top: 10px;
            }

            /* th:not(:nth-child(1)):not(:nth-child(2)):not(:nth-child(6)),
            td:not(:nth-child(1)):not(:nth-child(2)):not(:nth-child(6)) {
                display: none;
            } */
        }
    </style>
@endpush
