<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use Carbon\Carbon;
use App\Models\Tiket;

class ExportController extends Controller
{
    public function generateExcel(Request $request)
    {
        $date = $request->input('date');
        $formattedDate = date('Y-m-d', strtotime($date));

        $data = Tiket::whereDate('waktu_tiket', $formattedDate)->get()->map(function ($tiket) {
            foreach ($tiket->getAttributes() as $key => $value) {
                if ($value === null) {
                    $tiket->{$key} = '---';
                }
            }

            if ($tiket->nip_dosen !== '---') {
                $tiket->posisi = 'Dosen';
            } elseif ($tiket->nim_mahasiswa !== '---') {
                $tiket->posisi = 'Mahasiswa';
            } elseif ($tiket->nik_karyawan !== '---') {
                $tiket->posisi = 'Karyawan';
            } else {
                $tiket->posisi = 'Tidak Diketahui';
            }

            $tiket->waktu_tiket = Carbon::parse($tiket->waktu_tiket)->format('Y/m/d');

            return $tiket;
        });

        return (new FastExcel($data))->download('report.xlsx', function ($tiket) {
            return [
                'ID' => $tiket->id,
                'Nama' => $tiket->nama,
                'Posisi' => $tiket->posisi,
                'No. Tiket' => $tiket->no_tiket,
                'Email' => $tiket->email,
                'No. Telp' => $tiket->no_telepon,
                'Prodi' => $tiket->prodi,
                'Unit Kerja' => $tiket->unit_kerja_karyawan,
                'NIP' => $tiket->nip_dosen,
                'NIM' => $tiket->nim_mahasiswa,
                'NIK' => $tiket->nik_karyawan,
                'Judul Tiket' => $tiket->judul_tiket,
                'Urgensi' => $tiket->tingkat_urgensi,
                'Kategori' => $tiket->kategori_laporan,
                'Kategori (Jika Memilih "Lainnya")' => $tiket->kategori_lainnya,
                'Deskripsi' => $tiket->deskripsi_tiket,
                'Waktu' => $tiket->waktu_tiket,
                // 'Lokasi' => $tiket->lokasi_tiket,
                'Status' => $tiket->status_tiket,
            ];
        });
    }
}
