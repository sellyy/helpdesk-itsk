<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UmpanBalik;

class UmpanBalikController extends Controller
{
    public function tambahUmpanBalik(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|max:100',
            'status' => 'required|string|max:10',
            'email' => 'required|email|max:100',
            'no_telepon' => 'required|string|max:30',
            'pesan' => 'required|string',
        ]);

        UmpanBalik::create([
            'nama' => $request->input('nama'),
            'status' => $request->input('status'),
            'email' => $request->input('email'),
            'no_telepon' => $request->input('no_telepon'),
            'pesan' => $request->input('pesan'),
        ]);

        return redirect()->back()->with('success', 'Umpan balik telah berhasil dikirim!');
    }
}
