<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tiket;
use App\Models\LampiranTiket;
use Carbon\Carbon;

class TiketController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($role)
    {
        if ($role == 'dosen') {
            return view('lapor-dosen');
        } elseif ($role == 'karyawan') {
            return view('lapor-karyawan');
        } else {
            return view('lapor-mahasiswa');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $tiket = Tiket::create([
            'id_pengguna' => null,
            'nama' => $request->namalengkap,
            'email' => $request->email,
            'no_telepon' => $request->telp,
            'nim_mahasiswa' => $request->nim,
            'prodi' => $request->prodi,
            'nip_dosen' => $request->nip,
            'nik_karyawan' => $request->nik,
            'unit_kerja_karyawan' => $request->unitkerja,
            'judul_tiket' => $request->subjek,
            'deskripsi_tiket' => $request->deskripsi,
            'waktu_tiket' => Carbon::createFromFormat('Y-m-d H:i', $request->tanggal . ' ' . $request->waktu),
            'lokasi_tiket' => 'tes',
            'status_tiket' => 'Proses',
        ]);

        LampiranTiket::create([
            'id_tiket' => $tiket->id,
            'file_tiket' => $request->formFile,
        ]);

        // Respon dengan JSON atau pesan yang sesuai
        return response()->json(['success' => true, 'message' => 'Tiket berhasil disimpan.']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
