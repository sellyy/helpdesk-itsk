<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tiket extends Model
{
    use HasFactory;

    protected $table = 'tiket';
    protected $fillable = [
        'id_pengguna',
        'no_tiket',
        'nama',
        'email',
        'no_telepon',
        'nim_mahasiswa',
        'prodi',
        'nip_dosen',
        'nik_karyawan',
        'unit_kerja_karyawan',
        'judul_tiket',
        'tingkat_urgensi',
        'kategori_masalah',
        'kategori_lainnya',
        'deskripsi_tiket',
        'waktu_tiket',
        'lokasi_tiket',
        'status_tiket',
    ];

    public $timestamps = false;
    public $posisi;

    public function pengguna()
    {
        return $this->belongsTo(Pengguna::class);
    }

    public function riwayat()
    {
        return $this->hasMany(RiwayatTiket::class);
    }

    public function lampiran()
    {
        return $this->hasMany(LampiranTiket::class);
    }
}
