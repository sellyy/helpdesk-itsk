<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LampiranTiket extends Model
{
    use HasFactory;

    protected $table = 'lampiran_tiket';
    protected $fillable = [
        'id_tiket',
        'file_tiket',
    ];

    public $timestamps = false;

    public function tiket()
    {
        return $this->belongsTo(Tiket::class);
    }
}
