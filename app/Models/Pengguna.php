<?php

namespace App\Models;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Pengguna extends Authenticatable
{
    use HasFactory,Notifiable,CanResetPassword;

    protected $table = 'pengguna';
    protected $rememberTokenName = '';

    protected $fillable = [
        'email',
        'password',
        'role',
    ];

    public $timestamps = false;

    public function tiket()
    {
        return $this->hasMany(Tiket::class);
    }

    public function profil()
    {
        return $this->hasOne(Profil::class);
    }

    public function riwayat() {
        return $this->hasMany(RiwayatTiket::class);
    }
}
