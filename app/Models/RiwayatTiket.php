<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiwayatTiket extends Model
{
    use HasFactory;

    protected $table = 'riwayat_tiket';
    protected $fillable = [
        'id_pengguna',
        'id_tiket',
        'waktu_riwayat',
        'deskripsi_riwayat',
    ];

    public $timestamps = false;

    public function pengguna()
    {
        return $this->belongsTo(Pengguna::class);
    }

    public function tiket()
    {
        return $this->belongsTo(Tiket::class);
    }
}
