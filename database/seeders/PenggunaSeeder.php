<?php

namespace Database\Seeders;

use App\Models\Pengguna;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PenggunaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Pengguna::create([
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
            'role' => 'Admin',
        ]);
        Pengguna::create([
            'email' => 'teknisi@gmail.com',
            'password' => bcrypt('teknisi'),
            'role' => 'Teknisi',
        ]);
        Pengguna::create([
            'email' => 'superadmin@gmail.com',
            'password' => bcrypt('superadmin'),
            'role' => 'Super Admin',
        ]);
    }
}
