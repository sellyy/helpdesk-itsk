<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tiket', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_pengguna')->nullable();
            $table->string('no_tiket', 10);
            $table->string('nama', 100);
            $table->string('email', 100);
            $table->string('no_telepon', 30);
            $table->string('nim_mahasiswa', 50)->nullable();
            $table->enum('prodi', ['Informatika', 'Sistem Informasi', 'Jaringan Komputer'])->nullable();
            $table->string('nip_dosen', 50)->nullable();
            $table->string('nik_karyawan', 50)->nullable();
            $table->enum('unit_kerja_karyawan', ['Staff TU', 'OB'])->nullable();
            $table->string('judul_tiket', 100);
            $table->enum('tingkat_urgensi', ['Rendah', 'Sedang', 'Tinggi']);
            $table->enum('kategori_laporan', ['Teknis', 'Administratif', 'Akademis', 'Lainnya']);
            $table->string('kategori_lainnya')->nullable();
            $table->text('deskripsi_tiket');
            $table->datetime('waktu_tiket');
            $table->string('lokasi_tiket', 250);
            $table->enum('status_tiket', ['Belum Diproses', 'Sedang Diproses', 'Selesai']);

            $table->foreign('id_pengguna')->references('id')->on('pengguna');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tiket');
    }
};
